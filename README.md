# Tax app BE

### Prerequisites

1. Run npm install
2. Configure db in src/db
3. Run npm run init:db script to initialize necessary fields in DB (if it wasn't done before)

To clean DB run npm run clean:db

### Application launch

For development pruposes - npm run start:dev

Build application for release and run - npm run start
