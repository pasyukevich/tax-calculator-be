import pool from '../src/db';

const main = async () => {
    try {
        await pool.connect();
    } catch (e) {
        console.log(e);
    }
    try {
        await pool.query(
            `CREATE TABLE products (
       id SERIAL,
       name VARCHAR(100) UNIQUE,
       imported_product BOOLEAN,
       updated_at timestamp,
       cost DECIMAL(64,2),
       category VARCHAR(20)
  );`,
        );

        await pool.query(
            `CREATE TABLE tax_exceptions_category (
       type VARCHAR(10) UNIQUE
    );`,
        );

        await pool.query(`INSERT into tax_exceptions_category (type) VALUES ($1), ($2), ($3)`, [
            'sugar',
            'chocolate',
            'popcorn',
        ]);

        await pool.query(
            `CREATE TABLE taxes (
        name VARCHAR(20) UNIQUE,
        value DECIMAL(64,2)
    );`,
        );

        await pool.query(`INSERT into taxes (name, value) VALUES ($1, $2), ($3, $4)`, ['basic', 0.1, 'imported', 0.05]);

        await pool.query(
            `CREATE TABLE categories (
      name VARCHAR(20) UNIQUE );`,
        );

        await pool.query(`INSERT into categories (name) VALUES ($1), ($2), ($3)`, ['sugar', 'chocolate', 'popcorn']);
    } catch (e) {
        console.log(e);
        process.exit(1);
    }

    pool.end();
    process.exit();
};

main();
