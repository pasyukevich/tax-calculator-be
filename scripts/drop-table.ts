import pool from '../src/db';

const main = async () => {
    try {
        await pool.connect();
    } catch (e) {
        console.log(e);
    }

    try {
        await pool.query(`DROP TABLE products`);
    } catch (e) {
        console.log(e);
    }

    try {
        await pool.query(`DROP TABLE tax_exceptions_category`);
    } catch (e) {
        console.log(e);
    }

    try {
        await pool.query(`DROP TABLE taxes`);
    } catch (e) {
        console.log(e);
    }

    try {
        await pool.query(`DROP TABLE categories`);
    } catch (e) {
        console.log(e);
    }

    pool.end();
    process.exit();
};

main();
