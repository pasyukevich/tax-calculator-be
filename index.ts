import App from './App';

const port = parseInt(process.env.PORT || '4000');

const starter = new App()
    .start(port)
    .then((port) => console.log(`Running on port ${port}`))
    .catch((error) => {
        console.log(error);
    });

export default starter;
