export interface TaxDto {
    name: string;
    value: number;
}

export interface Taxes {
    basic: number;
    imported: number;
}
