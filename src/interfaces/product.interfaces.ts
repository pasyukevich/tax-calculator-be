export interface Poduct {
    id: number;
    name: string;
    importedProduct: boolean;
    updatedAt: Date;
    cost: number;
    category: string;
}

export interface ProductDto {
    id: number;
    name: string;
    imported_product: boolean;
    updated_at: Date;
    cost: number;
    category: string;
}
