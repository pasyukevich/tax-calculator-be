import { executeQuery } from '../services/sqlConnector';
import { PRODUCTS_SELECTOR_BY_ID, PRODUCTS_SELECTOR, ADD_PRODUCT, REMOVE_PRODUCT_BY_ID } from '../constants/sql';
import { replaceTemplateConstant } from '../utils';
import { Poduct } from '../interfaces';
import { mapProductsDto } from '../DTO';

export const productsRepository = {
    getAllProducts: async (): Promise<Poduct[]> => {
        const products = await executeQuery(PRODUCTS_SELECTOR);

        return mapProductsDto(products);
    },
    getProductsById: async (listIdsString: string): Promise<Poduct[]> => {
        const products = await executeQuery(replaceTemplateConstant(PRODUCTS_SELECTOR_BY_ID, listIdsString));

        return mapProductsDto(products);
    },
    addProduct: async (props: string[]): Promise<[]> => await executeQuery(ADD_PRODUCT, ...props),
    removeProduct: async (id: number): Promise<[]> =>
        await executeQuery(replaceTemplateConstant(REMOVE_PRODUCT_BY_ID, id)),
};
