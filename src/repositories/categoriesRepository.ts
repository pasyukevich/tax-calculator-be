import { ADD_CATEGORY, GET_CATEGORY_BY_NAME, REMOVE_CATEGORY_BY_NAME } from '../constants/sql';
import { executeQuery } from '../services/sqlConnector';
import { replaceTemplateConstant } from '../utils';

export const categoriesRepository = {
    getCategoryByName: async (name: string): Promise<any> =>
        await executeQuery(replaceTemplateConstant(GET_CATEGORY_BY_NAME, name)),
    addCategory: async (name: string): Promise<any> => await executeQuery(ADD_CATEGORY, name),
    removeCategory: async (name: string): Promise<any> =>
        await executeQuery(replaceTemplateConstant(REMOVE_CATEGORY_BY_NAME, name)),
};
