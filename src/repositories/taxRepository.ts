import { executeQuery } from '../services/sqlConnector';
import { TAXES_SELECTOR } from '../constants/sql';
import { mapTaxDto } from '../DTO';
import { Taxes } from '../interfaces';

export const taxRepository = {
    getTaxes: async (): Promise<Taxes> => {
        const taxes = await executeQuery(TAXES_SELECTOR);

        return mapTaxDto(taxes);
    },
};
