import {
    TAX_EXCEPTIONS_SELECTOR,
    ADD_TAX_EXCEPTION,
    REMOVE_TAX_EXCEPTION_BY_TYPE,
    GET_EXCEPTION_BY_TYPE,
} from '../constants/sql';
import { executeQuery } from '../services/sqlConnector';
import { replaceTemplateConstant } from '../utils';

export const taxExceptionsRepository = {
    getAllExceptions: async (): Promise<[]> => await executeQuery(TAX_EXCEPTIONS_SELECTOR),
    getExceptionByType: async (type: string): Promise<any> =>
        await executeQuery(replaceTemplateConstant(GET_EXCEPTION_BY_TYPE, type)),
    addException: async (type: string): Promise<any> => await executeQuery(ADD_TAX_EXCEPTION, type),
    removeException: async (type: string): Promise<any> =>
        await executeQuery(replaceTemplateConstant(REMOVE_TAX_EXCEPTION_BY_TYPE, type)),
};
