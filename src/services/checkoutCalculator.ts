import isEmpty from 'lodash/isEmpty';
import { roundTax } from '../utils';
import { BASIC_TAX_NAME, IMPORTED_TAX_NAME } from '../constants/sql';
import { taxExceptionsRepository, productsRepository, taxRepository } from '../repositories';
import { Poduct } from '../interfaces';

type CheckoutSummary = {
    products: Poduct[];
    tax: number;
    checkoutCost: number;
};

export const calculateCheckout = async (listIdsString: string): Promise<CheckoutSummary> => {
    const products = await productsRepository.getProductsById(listIdsString);

    const taxes = await taxRepository.getTaxes();

    const checkoutSummary = await products.reduce(async (acc: Promise<CheckoutSummary>, el: Poduct) => {
        const { cost, importedProduct, category } = el;
        const { tax, checkoutCost } = await acc;

        const taxException = await taxExceptionsRepository.getExceptionByType(category);

        const costNumber = Number(cost);

        const calculatedTax =
            (importedProduct ? roundTax(costNumber * taxes[IMPORTED_TAX_NAME]) : 0) +
            (!isEmpty(taxException) ? roundTax(costNumber * taxes[BASIC_TAX_NAME]) : 0);

        return { tax: tax + calculatedTax, checkoutCost: checkoutCost + costNumber + calculatedTax };
    }, Promise.resolve({ tax: 0, checkoutCost: 0 }));

    return { products, ...checkoutSummary };
};
