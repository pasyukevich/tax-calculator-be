import pool from '../db';

export const executeQuery = async (sqlQuery: string, ...params: string[]): Promise<[]> => {
    try {
        const client = await pool.connect();

        const { rows } = await client.query(sqlQuery, params.length !== 0 ? params : undefined);

        client.release();

        return rows;
    } catch (e) {
        console.log(e);
        throw new Error(`sqlConnector error: ${e.stack}`);
    }
};
