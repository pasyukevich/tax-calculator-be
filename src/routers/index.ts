import { Router } from 'express';
import { checkoutController, productController, taxExceptionsController, categoriesController } from '../controllers';

const router = Router();

router.get('/products', productController.getProducts);
router.post('/products/add', productController.addProduct);
router.delete('/products/remove', productController.removeProduct);
router.get('/exceptions', taxExceptionsController.getAllExceptions);
router.post('/exceptions/add', taxExceptionsController.addException);
router.delete('/exceptions/remove', taxExceptionsController.removeException);
router.post('/precheckout', checkoutController.getCheckoutSummary);
router.post('/category', categoriesController.getCategoryByName);
router.post('/category/add', categoriesController.addCategory);
router.delete('/category/remove', categoriesController.removeCategory);

export default router;
