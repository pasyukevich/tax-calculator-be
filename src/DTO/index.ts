import { ID, NAME, IMPORTED_PRODUCT, UPDATED_AT, COST, CATEGORY } from './constants';
import { Poduct, ProductDto, TaxDto, Taxes } from '../interfaces';
import { BASIC_TAX_NAME, IMPORTED_TAX_NAME } from '../constants/sql';

export const applyProductDto = ({
    [ID]: id,
    [NAME]: name,
    [IMPORTED_PRODUCT]: importedProduct,
    [UPDATED_AT]: updatedAt,
    [COST]: cost,
    [CATEGORY]: category,
}: ProductDto): Poduct => ({
    id,
    name,
    importedProduct,
    updatedAt,
    cost,
    category,
});

export const mapProductsDto = (products: ProductDto[]): Poduct[] => products.map((product) => applyProductDto(product));

export const mapTaxDto = (taxes: TaxDto[]): Taxes =>
    taxes.reduce(
        (acc, tax: TaxDto) => {
            const { name, value } = tax;

            return { ...acc, [name]: value };
        },
        { [BASIC_TAX_NAME]: 0, [IMPORTED_TAX_NAME]: 0 },
    );
