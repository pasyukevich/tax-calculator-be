export const ID = 'id';

export const NAME = 'name';

export const IMPORTED_PRODUCT = 'imported_product';

export const UPDATED_AT = 'updated_at';

export const COST = 'cost';

export const CATEGORY = 'category';
