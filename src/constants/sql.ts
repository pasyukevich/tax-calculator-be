export const PRODUCTS_SELECTOR = 'SELECT * FROM products';

export const PRODUCTS_SELECTOR_BY_ID = 'SELECT * FROM products WHERE id IN({{ids}})';

export const ADD_PRODUCT =
    'INSERT INTO products(name, imported_product, updated_at, cost, category) VALUES($1, $2, $3, $4, $5)';

export const REMOVE_PRODUCT_BY_ID = "DELETE FROM products WHERE id = '{{id}}'";

export const TAX_EXCEPTIONS_SELECTOR = 'SELECT * FROM tax_exceptions_category';

export const ADD_TAX_EXCEPTION = 'INSERT INTO tax_exceptions_category(type) VALUES($1)';

export const GET_EXCEPTION_BY_TYPE = "SELECT FROM tax_exceptions_category WHERE type = '{{type}}'";

export const REMOVE_TAX_EXCEPTION_BY_TYPE = "DELETE FROM tax_exceptions_category WHERE type = '{{type}}'";

export const GET_CATEGORY_BY_NAME = "SELECT FROM categories WHERE name = '{{name}}'";

export const ADD_CATEGORY = 'INSERT INTO categories(name) VALUES($1)';

export const REMOVE_CATEGORY_BY_NAME = "DELETE FROM tax_exceptions_category WHERE type = '{{type}}'";

export const TAXES_SELECTOR = 'SELECT * FROM taxes';

export const BASIC_TAX_NAME = 'basic';

export const IMPORTED_TAX_NAME = 'imported';
