import { Request, Response } from 'express';
import { STATUS_BAD_REQUEST } from '../constants/statusCodes';
import { productsRepository, categoriesRepository } from '../repositories';
import isEmpty from 'lodash/isEmpty';

export const productController = {
    getProducts: async (req: Request, res: Response): Promise<void> => {
        try {
            const products = await productsRepository.getAllProducts();

            res.send(products);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
    addProduct: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { name, imported_product, cost, category },
            } = req;

            const categoryFromDB = await categoriesRepository.getCategoryByName(category);

            if (isEmpty(categoryFromDB)) {
                res.status(STATUS_BAD_REQUEST).send('No such category');
                return;
            }

            const result = await productsRepository.addProduct([name, imported_product, new Date(), cost, category]);

            res.send(result);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
    removeProduct: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { id },
            } = req;

            const result = await productsRepository.removeProduct(id);

            res.send(result);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
};
