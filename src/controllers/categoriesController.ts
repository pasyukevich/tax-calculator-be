import { Request, Response } from 'express';
import { STATUS_BAD_REQUEST } from '../constants/statusCodes';
import { categoriesRepository } from '../repositories';

export const categoriesController = {
    getCategoryByName: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { name },
            } = req;

            const category = await categoriesRepository.getCategoryByName(name);

            res.send(category);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
    addCategory: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { name },
            } = req;

            const result = await categoriesRepository.addCategory(name);

            res.send(result);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
    removeCategory: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { name },
            } = req;

            const result = await categoriesRepository.removeCategory(name);

            res.send(result);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
};
