import { Request, Response } from 'express';
import { STATUS_BAD_REQUEST } from '../constants/statusCodes';
import { transformStringArrayToEnumeration } from '../utils';
import { calculateCheckout } from '../services/checkoutCalculator';

export const checkoutController = {
    getCheckoutSummary: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { listId },
            } = req;

            const listIdsString = transformStringArrayToEnumeration(listId);

            const checkoutSummary = await calculateCheckout(listIdsString);

            res.send(checkoutSummary);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
};
