import { Request, Response } from 'express';
import { STATUS_BAD_REQUEST } from '../constants/statusCodes';
import { taxExceptionsRepository } from '../repositories';

export const taxExceptionsController = {
    getAllExceptions: async (req: Request, res: Response): Promise<void> => {
        try {
            const result = await taxExceptionsRepository.getAllExceptions();

            res.send(result.map(({ type }) => type));
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
    addException: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { type },
            } = req;

            const result = await taxExceptionsRepository.addException(type);

            res.send(result);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
    removeException: async (req: Request, res: Response): Promise<void> => {
        try {
            const {
                body: { type },
            } = req;

            const result = await taxExceptionsRepository.removeException(type);

            res.send(result);
        } catch (error) {
            res.status(STATUS_BAD_REQUEST).send(error);
        }
    },
};
