export const replaceTemplateConstant = (str: string, value: string | number): string =>
    str.replace(/\{{[^)]*\}}/g, String(value));

export const roundTax = (val: number): number => {
    const lastSign = (val * 100) % 10;
    let roundedNumber = 0;

    if (lastSign !== 0) {
        if (lastSign <= 5) {
            roundedNumber = 5;
        } else {
            roundedNumber = 10;
        }
    }

    return (Math.trunc(val * 10) * 10 + roundedNumber) / 100;
};

export const transformStringArrayToEnumeration = (stringArray: string): string => JSON.parse(stringArray).join(',');
